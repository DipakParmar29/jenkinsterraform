

#!/bin/bash
sudo apt -y update

echo "Install Java JDK 8"
sudo apt remove -y java
sudo apt install -y java-1.8.0-openjdk

echo "Install Maven"
sudo apt install -y maven 

echo "Install git"
sudo apt install -y git

echo "Install Docker engine"
sudo apt update -y
apt install docker -y
sudo usermod -a -G docker jenkins
sudo service docker start
sudo chkconfig docker on

echo "Install Jenkins"
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo
rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
sudo apt install -y jenkins
sudo usermod -a -G docker jenkins
sudo chkconfig jenkins on
sudo service docker start
sudo service jenkins start

